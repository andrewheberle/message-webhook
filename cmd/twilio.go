package cmd

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/andrewheberle/message-webhook/providers/twilio"
)

var twilioCmd = &cobra.Command{
	Use:   "twilio",
	Short: "Run as a webhook endpoint for Twilio messages",
	Run: func(cmd *cobra.Command, args []string) {
		runTwilioCmd()
	},
}

func init() {
	rootCmd.AddCommand(twilioCmd)

	twilioCmd.Flags().String("auth", "12345", "auth token to verify signatures of webhook request")

	_ = viper.BindPFlags(twilioCmd.Flags())
}

func runTwilioCmd() {
	// initialise twilio provider
	provider, err := twilio.New(viper.GetString("auth"), viper.GetString("scheme"), viper.GetBool("dev"))
	if err != nil {
		log.Fatal().Err(err).Send()
	}

	if err := RunApp(provider); err != nil {
		log.Fatal().Err(err).Send()
	}
}
