package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

const version = "v2.0.5"

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of message-webhook",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("message-webhook %s\n", version)
	},
}
