package cmd

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/andrewheberle/message-webhook/providers/textmagic"
)

var textmagicCmd = &cobra.Command{
	Use:   "textmagic",
	Short: "Run as a webhook endpoint for TextMagic messages",
	Run: func(cmd *cobra.Command, args []string) {
		runTextMagicCmd()
	},
}

func init() {
	rootCmd.AddCommand(textmagicCmd)

	_ = viper.BindPFlags(textmagicCmd.Flags())
}

func runTextMagicCmd() {
	// initialise provider
	provider, err := textmagic.New(viper.GetString("auth"), viper.GetString("scheme"), viper.GetBool("dev"))
	if err != nil {
		log.Fatal().Err(err).Send()
	}

	if err := RunApp(provider); err != nil {
		log.Fatal().Err(err).Send()
	}
}
