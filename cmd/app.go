package cmd

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"html/template"
	"io/fs"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"gitlab.com/andrewheberle/message-webhook/message"
	"gitlab.com/andrewheberle/ubolt"
)

type App struct {
	db       *ubolt.BDB
	max      int
	ttl      time.Duration
	template *template.Template
	provider message.Provider
}

func NewApp(file string, max int, ttl time.Duration, provider message.Provider) (*App, error) {
	var tmpl *template.Template

	// open db
	db, err := ubolt.OpenB(file, []byte("messages"))
	if err != nil {
		return nil, err
	}

	// load templates
	if viper.GetBool("dev") {
		var err error

		tmpl, err = template.ParseGlob("templates/*.tmpl")
		if err != nil {
			return nil, err
		}
	} else {
		var err error

		tmpl, err = template.ParseFS(embedded, "templates/*.tmpl")
		if err != nil {
			return nil, err
		}
	}

	return &App{db: db, max: max, ttl: ttl, template: tmpl, provider: provider}, nil
}

func (app *App) Close() error {
	return app.db.Close()
}

func (app *App) CleanUp(broken bool) error {
	// list of keys to delete
	toDelete := make([][]byte, 0)

	// retrieve data in reverse order
	n := 0
	keys := app.db.GetKeys()
	for i := len(keys) - 1; i >= 0; i-- {
		var msg message.Message

		// get value
		v, err := app.db.GetE(keys[i])
		if err != nil {
			// skip errors
			continue
		}

		// attempt to unmarshal data from key
		if err := json.Unmarshal(v, &msg); err != nil {
			log.Info().Err(err).Str("key", string(keys[i])).Msg("json error")

			// if set add broken keys to list
			if broken {
				toDelete = append(toDelete, keys[i])
			}
			continue
		}

		// increment counter (only care about valid keys)
		n++

		// if we are past max and the ttl is expired add to keys to be deleted
		if n > app.max && time.Since(msg.Timestamp) > app.ttl {
			toDelete = append(toDelete, keys[i])
		}
	}

	// at this point delete expired keys
	for _, k := range toDelete {
		if err := app.db.Delete(k); err != nil {
			log.Info().Err(err).Str("key", string(k)).Msg("delete error")
		} else {
			log.Info().Str("key", string(k)).Msg("deleted")
		}
	}

	return nil
}

func (app *App) HandleMessage(w http.ResponseWriter, r *http.Request) {
	// marshal message into json
	data, err := app.provider.Marshal(r)
	if err != nil {
		message.HttpError(w, r, "500 Internal Server Error", err.Error(), http.StatusInternalServerError)
		return
	}

	// write into database
	if err := app.db.Put(nil, data); err != nil {
		message.HttpError(w, r, "500 Internal Server Error", err.Error(), http.StatusInternalServerError)
		return
	}

	// send response back
	app.provider.Response(w, r)
}

func (app *App) MessageList(w http.ResponseWriter, r *http.Request) {
	var messages message.List
	var seeker []byte

	if seekto := r.FormValue("seekto"); seekto != "" {
		var err error
		seeker, err = base64.StdEncoding.DecodeString(seekto)
		if err != nil {
			message.HttpError(w, r, "400 Bad Request", err.Error(), http.StatusBadRequest)
			return
		}
	}

	// retrieve data in reverse order
	n := 0
	keys := app.db.GetKeys()
	for i := len(keys) - 1; i >= 0; i-- {
		var msg message.Message

		if seeker != nil {
			if bytes.Equal(seeker, keys[i]) {
				seeker = nil
				continue
			}

			continue
		}

		// get value
		v, err := app.db.GetE(keys[i])
		if err != nil {
			// skip errors
			continue
		}

		// attempt to unmarshal data from key
		if err := json.Unmarshal(v, &msg); err != nil {
			log.Info().Err(err).Str("key", string(keys[i])).Msg("json error")
			continue
		}

		// append to list
		messages.Messages = append(messages.Messages, msg)

		// increment count
		n++

		// if have hit max then do paging
		if n == app.max {
			nextlink := r.URL
			nextlink.RawQuery = "seekto=" + base64.StdEncoding.EncodeToString(keys[i])
			messages.NextLink = nextlink.String()
			break
		}
	}

	// set message count
	messages.Count = len(messages.Messages)

	// convert to json
	data, err := json.Marshal(messages)
	if err != nil {
		message.HttpError(w, r, "500 Internal Server Error", err.Error(), http.StatusInternalServerError)
		return
	}

	// write back to client
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(data)
}

func (app *App) HealthCheck(w http.ResponseWriter, r *http.Request) {
	// check service health by attempting to open bucket
	if err := app.db.Ping(); err != nil {
		// for any error return a non-200 response
		message.HttpError(w, r, "500 Internal Server Error", err.Error(), http.StatusInternalServerError)
		return
	}

	// we got here so 200 response == ok
	message.HttpStatusOK(w, r)
}

func templatepath(pathname string) bool {
	if pathname == "/" ||
		pathname == "/index.html" ||
		pathname == "/index-jquery.html" ||
		pathname == "/index-vue.html" ||
		pathname == "/index-alpinejs.html" {
		return true
	}

	return false
}

func templatename(pathname string) string {
	if pathname == "/" || pathname == "/index.html" {
		return "index-alpinejs.html.tmpl"
	}

	switch pathname {
	case "index-jquery.html":
		return "index-jquery.html.tmpl"
	case "index-vue.html":
		return "index-vue.html.tmpl"
	case "index-alpinejs.html":
		return "index-alpinejs.html.tmpl"
	}

	return "missing.tmpl"
}

func (app *App) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if templatepath(r.URL.Path) {

		// build data for template
		data := struct {
			Title       string
			Hash        string
			Scheme      string
			ServerName  string
			Logo        string
			LogoSvg     string
			DarkLogo    string
			DarkLogoSvg string
			Nonce       string
		}{
			Title:       viper.GetString("branding.title"),
			Logo:        viper.GetString("branding.logo"),
			LogoSvg:     viper.GetString("branding.logosvg"),
			DarkLogo:    viper.GetString("branding.darklogo"),
			DarkLogoSvg: viper.GetString("branding.darklogosvg"),
			Scheme:      viper.GetString("scheme"),
			ServerName:  r.Host,
		}

		if viper.GetBool("dev") {
			// set hash and csp nonce based on time in dev mode
			data.Hash = fmt.Sprintf("%d", time.Now().UnixNano())
			data.Nonce = data.Hash
		} else {
			// set hash based on version and nonce to use real value set by csp middleware
			data.Hash = base64.StdEncoding.EncodeToString([]byte(version))
			data.Nonce = NonceFromContext(r.Context())
		}

		// set template name
		name := templatename(r.URL.Path)

		// execute template
		if err := app.template.ExecuteTemplate(w, name, data); err != nil {
			log.Error().Err(err).Send()
		}
	} else {
		var fshandler http.FileSystem

		// return 404 for paths ending in "/" to prevent directory listings
		if strings.HasSuffix(r.URL.Path, "/") {
			http.NotFound(w, r)
			return
		}

		// otherwise handle using a fileserver handler
		if viper.GetBool("dev") {
			fshandler = http.Dir("static")
		} else {
			fsys, err := fs.Sub(embedded, "static")
			if err != nil {
				log.Fatal().Err(err).Send()
			}

			fshandler = http.FS(fsys)
		}

		// run ServeHTTP method of handler
		http.FileServer(fshandler).ServeHTTP(w, r)
	}
}

func RunApp(provider message.Provider) error {
	// init app
	app, err := NewApp(viper.GetString("db"), viper.GetInt("max"), viper.GetDuration("ttl"), provider)
	if err != nil {
		return err
	}
	defer app.Close()

	// set up expiry of old keys
	if viper.IsSet("clean") {
		go func() {
			for {
				time.Sleep(viper.GetDuration("clean"))
				if err := app.CleanUp(true); err != nil {
					log.Info().Err(err).Send()
				}
			}
		}()
	}

	// set up router
	r := mux.NewRouter()

	// api endpoints
	webhook := r.PathPrefix("/message").Subrouter()
	webhook.HandleFunc("", app.HandleMessage).Methods(http.MethodGet, http.MethodPost)
	webhook.Use(app.provider.VerifyMiddleware)

	r.HandleFunc("/list", app.MessageList).Methods(http.MethodGet)
	r.HandleFunc("/healthz", app.HealthCheck).Methods(http.MethodGet)

	// set up csp middleware when running in production
	if !viper.GetBool("dev") {
		r.Use(cspMiddleware)
	}

	// handler for static files
	r.PathPrefix("/").Handler(app)

	// start web service
	if viper.IsSet("cert") && viper.IsSet("key") {
		// https
		viper.Set("scheme", "https")
		log.Info().Str("addr", viper.GetString("addr")).Str("scheme", viper.GetString("scheme")).Msg("starting server")
		return http.ListenAndServeTLS(viper.GetString("addr"), viper.GetString("cert"), viper.GetString("key"), r)
	}

	// http
	log.Info().Str("addr", viper.GetString("addr")).Str("scheme", viper.GetString("scheme")).Msg("starting server")
	return http.ListenAndServe(viper.GetString("addr"), r)
}
