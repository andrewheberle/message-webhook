package cmd

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"net/http"
)

const nonceKey int = iota

func NewNonce() (string, error) {
	b := make([]byte, 16)
	if _, err := io.ReadFull(rand.Reader, b); err != nil {
		return "", err
	}

	nonce := base64.RawStdEncoding.EncodeToString(b)

	return nonce, nil
}

func NonceFromContext(ctx context.Context) string {
	if val, ok := ctx.Value(nonceKey).(string); ok {
		return val
	}

	return ""
}

func WithNonce(ctx context.Context, n string) context.Context {
	return context.WithValue(ctx, nonceKey, n)
}

func cspMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// calculate nonce for reqest
		nonce, err := NewNonce()
		if err != nil {
			http.Error(w, "500 Internal Server Error", http.StatusInternalServerError)
		}

		// add csp headers
		w.Header().Set("Content-Security-Policy", fmt.Sprintf("default-src 'self'; script-src 'self' 'nonce-%s' 'unsafe-eval' cdnjs.cloudflare.com; style-src 'self' cdnjs.cloudflare.com;", nonce))

		// add nonce to context
		ctx := WithNonce(r.Context(), nonce)
		r = r.WithContext(ctx)

		// call the next handler
		next.ServeHTTP(w, r)
	})
}
