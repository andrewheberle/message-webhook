package cmd

import (
	"embed"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	rootCmd = &cobra.Command{
		Use:   "message-webhook",
		Short: "This service provides a webhook endpoint for supported API driven SMS services",
	}

	embedded embed.FS
)

func Execute(fs embed.FS) {
	embedded = fs
	if err := rootCmd.Execute(); err != nil {
		log.Fatal().Err(err).Send()
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().String("addr", ":8080", "listen address")
	rootCmd.PersistentFlags().String("cert", "", "tls certificate")
	rootCmd.PersistentFlags().Duration("clean", time.Hour*24, "duration between cleans of database")
	rootCmd.PersistentFlags().String("config", "", "config file (optional)")
	rootCmd.PersistentFlags().String("db", "data.db", "database file location")
	rootCmd.PersistentFlags().Bool("debug", false, "enable debug mode")
	rootCmd.PersistentFlags().Bool("dev", false, "enable development mode")
	rootCmd.PersistentFlags().String("key", "", "tls private key")
	rootCmd.PersistentFlags().Int("max", 100, "maximum results from api before paging occurs")
	rootCmd.PersistentFlags().Duration("ttl", time.Hour*672, "maximum ttl for messages before expiry")

	_ = viper.BindPFlags(rootCmd.PersistentFlags())
}

func initConfig() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	// set some initial defaults
	viper.SetDefault("scheme", "http")
	viper.SetDefault("branding.title", "Messages")
	viper.SetDefault("branding.logo", "/img/logo.png")
	viper.SetDefault("branding.logosvg", "/img/logo.svg")
	viper.SetDefault("branding.darklogo", "/img/logo-dark.png")
	viper.SetDefault("branding.darklogosvg", "/img/logo-dark.svg")

	// set up env vars
	viper.SetEnvPrefix("mw")
	viper.AutomaticEnv()

	// set up debug mode
	if viper.GetBool("debug") {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	// load config file if specified
	if viper.IsSet("config") {
		viper.SetConfigFile(viper.GetString("config"))
		if err := viper.ReadInConfig(); err != nil {
			log.Fatal().Err(err).Str("config", viper.GetString("config")).Send()
		}
	}
}
