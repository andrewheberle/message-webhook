package textmagic

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/andrewheberle/message-webhook/message"
)

type Provider struct {
}

type Message struct {
	Id          string `json:"id"`
	Sender      string `json:"sender"`
	Receiver    string `json:"receiver"`
	MessageTime string `json:"messageTime"`
	Text        string `json:"text"`
}

func New(auth, scheme string, dev bool) (*Provider, error) {
	switch scheme {
	case "":
		return &Provider{}, nil
	case "http", "https":
		return &Provider{}, nil

	}

	return nil, fmt.Errorf("invalid scheme: %s", scheme)
}

func (pr *Provider) VerifyMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// no validation is done here

		// pass to next handler
		next.ServeHTTP(w, r)
	})
}

func (pr *Provider) Response(w http.ResponseWriter, r *http.Request) {
	// send response back
	w.WriteHeader(http.StatusOK)
}

func (pr *Provider) Marshal(r *http.Request) ([]byte, error) {
	var msg Message

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&msg); err != nil {
		return nil, err
	}

	return json.Marshal(message.Message{
		To:        msg.Receiver,
		From:      msg.Sender,
		Body:      msg.Text,
		Timestamp: time.Now(),
	})
}
