package twilio

import (
	"sort"
	"strings"
)

func concatValues(s map[string][]string) string {
	var result string

	if s == nil {
		return ""
	}

	keys := make([]string, 0, len(s))
	for k := range s {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		result = result + k + strings.Join(s[k], "")
	}

	return result
}
