package twilio

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/andrewheberle/message-webhook/message"
)

type TwilioProvider struct {
	dev    bool
	auth   string
	scheme string
}

func New(auth, scheme string, dev bool) (*TwilioProvider, error) {
	switch scheme {
	case "":
		return &TwilioProvider{auth: auth, scheme: "http", dev: dev}, nil
	case "http", "https":
		return &TwilioProvider{auth: auth, scheme: scheme, dev: dev}, nil

	}

	return nil, fmt.Errorf("invalid scheme: %s", scheme)
}

func (pr *TwilioProvider) VerifyMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// validate incoming request
		for _, key := range []string{"To", "From", "Body"} {
			if r.FormValue(key) == "" {
				message.HttpError(w, r, "400 Bad Request", "missing values", http.StatusBadRequest)
				return
			}
		}

		// do hmac verification when not in dev mode
		if !pr.dev {
			signature := r.Header.Get("X-Twilio-Signature")

			// if signature is not present result is invalid
			if signature == "" {
				message.HttpError(w, r, "403 Forbidden", "empty signature", http.StatusForbidden)
				return
			}

			// decode base64 signature
			messageMAC, err := base64.StdEncoding.DecodeString(signature)
			if err != nil {
				message.HttpError(w, r, "400 Bad Request", err.Error(), http.StatusBadRequest)
				return
			}

			// parse post values
			if r.Method == http.MethodPost {
				if err := r.ParseForm(); err != nil {
					message.HttpError(w, r, "400 Bad Request", err.Error(), http.StatusBadRequest)
					return
				}
			}

			// generate HMAC and verify
			mac := hmac.New(sha1.New, []byte(pr.auth))
			p := []byte(pr.scheme + "://" + r.Host + r.URL.String() + concatValues(r.PostForm))
			_, _ = mac.Write(p)

			expectedMAC := mac.Sum(nil)
			if !hmac.Equal([]byte(messageMAC), expectedMAC) {
				message.HttpError(w, r, "403 Forbidden", "invalid signature", http.StatusForbidden)
				return
			}
		}

		// pass to next handler
		next.ServeHTTP(w, r)
	})
}

func (pr *TwilioProvider) Response(w http.ResponseWriter, r *http.Request) {
	// send response back
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-type", "text/xml")
	fmt.Fprintf(w, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<Response></Response>\n")
}

func (pr *TwilioProvider) Marshal(r *http.Request) ([]byte, error) {
	return json.Marshal(message.Message{
		To:        r.FormValue("To"),
		From:      r.FormValue("From"),
		Body:      r.FormValue("Body"),
		Timestamp: time.Now(),
	})
}
