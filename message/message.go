package message

import (
	"net/http"
	"time"

	"github.com/rs/zerolog/log"
)

type Message struct {
	From      string    `json:"from"`
	To        string    `json:"to"`
	Body      string    `json:"body"`
	Timestamp time.Time `json:"timestamp"`
}

type List struct {
	NextLink string    `json:"@odata.nextLink,omitempty"`
	Count    int       `json:"@odata.count"`
	Messages []Message `json:"value"`
}

type Provider interface {
	Marshal(r *http.Request) ([]byte, error)
	Response(w http.ResponseWriter, r *http.Request)
	VerifyMiddleware(next http.Handler) http.Handler
}

func HttpStatusOK(w http.ResponseWriter, r *http.Request) {
	HttpError(w, r, "200 OK", "", http.StatusOK)
}

func HttpError(w http.ResponseWriter, r *http.Request, err, msg string, code int) {
	http.Error(w, err, code)
	log.Info().
		Str("method", r.Method).
		Str("proto", r.Proto).
		Str("path", r.URL.Path).
		Str("remote", r.RemoteAddr).
		Int("status", code).
		Msg(msg)
}
