const app = Vue.createApp({
    data: function() { 
        return {
            isVisible: false,
            messages: [],
        }
    },

    created: function() {
        this.fetchData();
    },

    methods: {
        fetchData: function() {
            this.messages = [];
            axios
                .get("/list")
                .then(response => {
                    this.isVisible = true;
                    this.messages = response.data.value;
                })
                .catch(error => {
                    this.messages = [];
                });
        },
        handleNewlines: function(v) {
            return v.replace(/(?:\r\n|\r|\n)/g, '<br>')
        }
    }
});

app.mount('#content');
