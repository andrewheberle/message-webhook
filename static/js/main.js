$( document ).ready(function() {
    // graba data from api
    $.get( "/list", function( data ) {
        data.value.forEach(msg => {
            $("#content").append(`<div class="row justify-content-center">
                <div class="col-12 col-sm-11 col-lg-8">
                    <div class="card mt-2">
                        <div class="card-body">
                            <h5 class="card-title">${msg.from}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">${msg.timestamp}</h6>
                            <p class="card-text">${msg.body.replace(/(?:\r\n|\r|\n)/g, '<br>')}</p>
                        </div>
                    </div>
                </div>
            </div>`);
        });
    });

    // make visible
    $("#content").addClass("message-visible");
});
