module gitlab.com/andrewheberle/message-webhook

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/rs/zerolog v1.25.0
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
	gitlab.com/andrewheberle/ubolt v1.4.2
	golang.org/x/sys v0.0.0-20210921065528-437939a70204 // indirect
)
