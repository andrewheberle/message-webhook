package main

import (
	"embed"

	"gitlab.com/andrewheberle/message-webhook/cmd"
)

//go:embed static templates
var fs embed.FS

func main() {
	cmd.Execute(fs)
}
