FROM golang:1.16 AS builder

COPY . /build

RUN cd /build && \
    CGO_ENABLED=0 go build -o /build/message-webhook .

FROM gcr.io/distroless/static-debian10

COPY --from=builder /build/message-webhook /app/message-webhook

EXPOSE 8080

WORKDIR /app

ENTRYPOINT [ "/app/message-webhook" ]