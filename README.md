# A Web Hook Service for SMS Providers

This is used to run a web hook service for incoming messages from SMS/Messaging providers.

Currently supports:

* Twilio
* TextMagic

Incoming messages are persisted in a [bbolt](https://github.com/etcd-io/bbolt) key/value store and can be viewed by the very basic included web frontend.

## Installation

```sh
go get gitlab.com/andrewheberle/message-webhook@v2.0.5
message-provider --db /path/to/data.db PROVIDER
```

## Docker

```sh
docker run \
    -v /path/to/data:/app/data \
    -e MW_DB=/app/data/data.db \
    -p 8080:8080 \
    registry.gitlab.com/andrewheberle/message-webhook:v2.0.5 \
    PROVIDER
```

## API

The service exposes three API endpoints:

| Endpoint  | Methods     | Response |
|-----------|-------------|----------|
| /list     | GET         | JSON list of messages in reverse date order (newest first) |
| /message  | GET or POST | Webhook endpoint |
| /healthz  | GET         | Health check endpoint. Responds with 200 HTTP status if service is OK |

The `/message` endpoint expects a valid request for the provider.

Only the following message fields are saved into the database at this time:

* To
* From
* Body

In addition a timestamp is written to the database.

### /list response

The `/list` endpoint responds with the following JSON:

```json
{
    "@odata.count":4,
    "value":[
        {
            "from":"+61555112233",
            "to":"+61555445566",
            "body":"OK, I'm all good now...",
            "timestamp":"2021-09-14T13:50:02.486117Z"
        },
        {
            "from":"+61555112233",
            "to":"+61555445566",
            "body":"I'm sorry baby...\n\nI just get scared sometimes...please forgive me...",
            "timestamp":"2021-09-14T16:28:06.5619969+08:00"
        },
        {
            "from":"+61555112233",
            "to":"+61555445566",
            "body":"WHY ARE YOU IGNORING ME??!!!???",
            "timestamp":"2021-09-14T15:28:26.5373711+08:00"
        },
        {
            "from":"+61555112233",
            "to":"+61555445566",
            "body":"Are you there??",
            "timestamp":"2021-09-14T14:47:12.939477+08:00"
        }
    ]
}
```

If the JSON response include more than the configured paging value (this currently defaults to 100 messages), the JSON response will include and `@odata.nextLink` value with a URL to retreive the next batch of messages.

## Command Line

```sh
message-webhook [GLOBAL OPTIONS] PROVIDER [PROVIDER OPTIONS]
```

The following command line arguments are supported:

| Flag  | Type     | Environment Variable | Default        | Info |
|-------|----------|----------------------|----------------|------|
| addr  | string   | MW_ADDR              | :8080          | HTTP listen address |
| auth  | string   | MW_AUTH              | 1234           | Authentication token used to verify request to webhook |
| cert  | string   | MW_CERT              | (no default)   | Certificate for enabling HTTPS (key must also be specified) |
| clean | duration | MW_CLEAN             | 24h            | Interval between message clean-up in database. Set to zero to disable |
| db    | string   | MW_DB                | data.db        | Location of database |
| debug | bool     | MW_DEBUG             | false          | Enable some extra debug logging |
| dev   | bool     | MW_DEV               | false          | Enables development mode |
| key   | string   | MW_KEY               | (no default)   | Key for enabling HTTPS (certificate must also be specified) |
| max   | int      | MW_MAX               | 100            | Maximum results from api before paging occurs |
| ttl   | duration | MW_TTL               | 672h (28 days) | Maximum ttl for messages before expiry |

All command line options may also be set via environment variables.

## Reverse Proxy

If the service is running in HTTP mode and exposed behind a reverse proxy that performs SSL termination, it is possible to set the `MW_SCHEME` environment variable to ensure the verification process of the webhook wendpoint works correctly:

```sh
docker run \
    -v /path/to/data:/app/data \
    -e MW_DB=/app/data/data.db \
    -e MW_SCHEME=https \
    -p 8080:8080 \
    registry.gitlab.com/andrewheberle/message-webhook:v2.0.5 \
    PROVIDER
```

## Cleaning

A background process runs based on the configured "clean" interval to remove messages older than the configured value.

The process will also remove keys from the database that have invalid content (in case of corruption or some manual changes to the K/V store).

Messages are only removed once there are more than the configured paging/max value.

## Development Mode

When the `--dev` command line switch or the `MW_DEV` environment variable is set to `true`, the service runs in "development mode", which changes the behaviour of the service in the following ways:

* Incoming webhook requests to the `/message` endpoint are not verified
* Static content and templates are served from the local fileystem rather than from the embedded versions
* No `Content-Security-Policy` is set on HTTP reponses

## Config File

All options set via command line flags or environment variables may also be set in a YAML based configuration file.

In addition some minor branding changes can be applied via the configuration file as follows:

```yaml
---
auth: '1234'
scheme: https
branding:
  logo: /path/to/logo.png
  logosvg: /path/to/logo.svg
  title: My Awesome Site
```
